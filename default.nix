{ mkDerivation, aeson, base, bytestring, containers, hriemann
, network, optparse-applicative, stdenv, text, uri-encode
, websockets
}:
mkDerivation {
  pname = "rcl";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base containers hriemann network optparse-applicative text
    uri-encode websockets
  ];
  executableHaskellDepends = [
    aeson base bytestring hriemann network optparse-applicative text
    uri-encode websockets
  ];
  license = stdenv.lib.licenses.bsd3;
}
