{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ViewPatterns               #-}

module Rcl
  ( EventName(..)
  , Attrs(..)
  , sendEventAttrs
  , sendEventFull
  ) where

import           Data.Aeson
import           Data.Map.Strict                         (Map)
import qualified Data.Map.Strict                         as Map
import           Data.Monoid
import           Data.String                             (IsString)
import           GHC.Generics                            (Generic)
import           Network.Monitoring.Riemann.Client       (Client (..))
import           Network.Monitoring.Riemann.Event        (Event, attribute)
import qualified Network.Monitoring.Riemann.Event.Monoid as EventM

newtype EventName = EventName { unEvtName :: String } deriving (Show, Eq, IsString)

newtype Attrs = Attrs (Map String String) deriving (Show, Eq, Generic, ToJSON, FromJSON)

toAttributes :: Attrs -> Endo Event
toAttributes (Attrs m) = EventM.attributes $ Map.foldMapWithKey mkAttr m
  where
    mkAttr k v = [attribute k (Just v)]

sendEventAttrs :: Client client => client -> EventName -> Attrs -> IO ()
sendEventAttrs c (unEvtName -> svc) attrs = do
  evt <- (toAttributes attrs <>) <$> EventM.timeAndHost
  sendEvent c $ EventM.ok svc evt

sendEventFull :: Client client => client -> Event -> IO ()
sendEventFull c event = do
  evt <- EventM.append event <$> EventM.timeAndHost
  sendEvent c evt

